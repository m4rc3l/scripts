# Scripts

Some more or less useful scripts.

## Certificate (+Authority)

Generate a certificate authority and additional certificates.

```sh
# generate a new ca
./ca.sh <ca-name>
# you will be asked some questions about the new ca

# generate a certificate and sign it with the new ca
./cer.sh <ca-name> <cer-name> [domains]
# domains is a list:
./cer.sh my-ca my-cer example.org www.example.org
```
