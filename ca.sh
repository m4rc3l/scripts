#!/bin/bash

name="${1}"

openssl ecparam -out "./${name}.key" -name secp384r1 -genkey
openssl req -x509 -new -nodes -extensions v3_ca -key "./${name}.key" -days 3652 -out "./${name}.cer" -sha512
