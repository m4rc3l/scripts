#!/bin/sh

ca="${1}"
name="${2}"

echo "subjectAltName = @alt_names

[alt_names]" >tmp

i=0
for domain in "$@"; do
  if [ $i -gt 1 ]; then
    echo "DNS.$i = ${domain}" >>tmp
  fi
  i=$((i + 1))
done

openssl ecparam -out "./${name}.key" -name secp384r1 -genkey
openssl req -new -key "./${name}.key" -out "./${name}.csr" -sha512
openssl x509 -req -days 3652 -CAkey "./${ca}.key" -CA "./${ca}.cer" -CAcreateserial -in "./${name}.csr" -out "./${name}.cer" -sha512 -extfile tmp

cat "./${ca}.cer" >>"./${name}.cer"
